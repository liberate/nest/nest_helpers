require 'bootstrap_icons/bootstrap_icon'

module NestHelpers
  class CustomIcon < BootstrapIcons::BootstrapIcon
    def find_bootstrap_icon
      icon = NestHelpers::IconDefinitions::ICONS[symbol]
      raise_icon_not_found if icon.nil?
      icon
    end
  end
end