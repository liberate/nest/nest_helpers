require "nest_helpers/engine"
require "nest_helpers/exceptions"

module NestHelpers
  autoload :CustomIcon, 'nest_helpers/custom_icon'
  autoload :IconDefinitions, 'nest_helpers/icon_definitions'
end
