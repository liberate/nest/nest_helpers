module NestHelpers
  module MarkdownHelper

    ALLOWED_TAGS        = %w[a strong em b i span img br code p div blockquote pre li ul ol h1 h2 h3 h4 h5]
    ALLOWED_ATTRIBUTES  = %w[href target src]
    INLINE_TAGS         = %w[a strong em b i u span img br code]

    #
    # This helper displays markdown text as html, with the string marked
    # as html safe so it will display.
    #
    # It will use Kramdown if installed, or Redcarpet if Kramdown is not available,
    # or will fall back on just sanitizing the input.
    #
    # html is notoriously difficult to sanitize safely, and markdown can include
    # html in it, so there be dragons here.
    #
    # for this reason, we first pass the raw input text through rails sanitize()
    # before it is passed to the markdown processor. This has the unfortunate
    # consequence of removing some valid markdown, such as "<https://example.org>"
    #
    # @param str [String] The string of markdown to convert.
    # @param inline [Boolean] inline If true, don't include any block HTML elements (default: false).
    # @param tags [Array<String>] If set, limit allowed tags to ones in this list.
    # @param attributes [Array<String>] If set, limit allowed attributes to ones in this list.
    #
    # @return [String] The HTML generated.
    #
    def markdown(str, inline: false, tags: nil, attributes: nil)
      str = sanitize(str,
        tags: (tags || ALLOWED_TAGS),
        attributes: (attributes || ALLOWED_ATTRIBUTES)
      )
      if defined?(Kramdown)
        result = Kramdown::Document.new(str).to_html
      elsif defined?(Redcarpet)
        result = Redcarpet::Markdown.new(
          Redcarpet::Render::HTML.new(
            filter_html: true, no_images: true,
            no_styles: true, safe_links_only: true
          ),
          autolink: true
        ).render(str)
      else
        result = str
      end
      if inline
        return sanitize(
          result,
          tags: INLINE_TAGS,
          attributes: (attributes || ALLOWED_ATTRIBUTES)
        ).html_safe
      else
        return result.html_safe
      end
    end

    #
    # convert markdown to look a bit more like ordinary plain text:
    #
    # 1. Convert the links in markdown to more friendly footnote links
    # 2. Remove inline formatting
    # 3. Ensure no double returns
    #
    def markdown_to_text(str, base_url: nil)
      str = str.dup

      # convert links to footnotes
      regexp = Regexp.new(/\[([^\]]+)\]\(([^\)]+)\)/)
      current_footnote = 0
      footnotes = []
      str.gsub!(regexp) do |link|
        match = link.match(regexp)
        text = match[1]
        url = normalize_url(path: match[2], base: base_url)
        this_footnote = current_footnote
        current_footnote += 1
        footnotes[this_footnote] = url
        "#{text} [#{this_footnote}]"
      end
      if footnotes.any?
        str.concat("\n\n")
        footnotes.each_with_index do |url, i|
           str.concat("[#{i}] #{url}\n")
        end
      end

      # remove inline formatting
      str.gsub!(/(\*\*+|__+)/, '')

      # remove double returns
      str.gsub!(/\n\n\n+/, "\n\n")

      return str
    end

    def normalize_url(base:nil, path:nil)
      base ||= ""
      path ||= ""
      if path =~ /^\//
        if base != "" && base !~ /^http/
          if base =~ /localhost/
            base = "http://" + base
          else
            base = "https://" + base
          end
        end
        url = [base, path].join('/')
      else
        url = path
      end
      url = url.gsub(/(([^:])\/\/+)/, '\2/') # remove duplicate /
      return url
    end

  end
end