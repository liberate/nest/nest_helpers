# encoding: utf-8
#
# adapted from https://github.com/tenderlove/rails_autolink, with minor
# modifications to support current rails and markdown.
#
# MIT license
#
# TODO:
#
# * consider using https://github.com/sporkmonger/addressable to clean up URLs
#   (for example, URLs with ../.. that can be nefarious)
#
module NestHelpers
  module AutolinkHelper

    def autolink(str, html_options={}, options={}, &block)
      auto_link_urls(str, html_options, options, &block)
    end

    private

    AUTO_LINK_RE = %r{
        (?: ((?:ed2k|ftp|http|https|irc|mailto|news|gopher|nntp|telnet|webcal|xmpp|callto|feed|svn|urn|aim|rsync|tag|ssh|sftp|rtsp|afs|file):)// | www\.\w )
        [^\s<\u00A0"]+
      }ix

    # regexps for determining context, used high-volume
    AUTO_LINK_CRE = [/<[^>]+$/, /^[^>]*>/, /<a\b.*?>/i, /<\/a>/i]

    AUTO_EMAIL_LOCAL_RE = /[\w.!#\$%&'*\/=?^`{|}~+-]/
    AUTO_EMAIL_RE = /(?<!#{AUTO_EMAIL_LOCAL_RE})[\w.!#\$%+-]\.?#{AUTO_EMAIL_LOCAL_RE}*@[\w-]+(?:\.[\w-]+)+/

    BRACKETS = { ']' => '[', ')' => '(', '}' => '{' }

    WORD_PATTERN = RUBY_VERSION < '1.9' ? '\w' : '\p{Word}'

    BLOCKDOWN_LINK_RE = /[^\]]\]\(.+\)/ # matches "https:example.org](example)"

    # Turns all urls into clickable links. If a block is given, each url
    # is yielded and the result is used as the link text.
    def auto_link_urls(text, html_options = {}, options = {})
      link_attributes = html_options.stringify_keys
      text.gsub(AUTO_LINK_RE) do
        scheme, href = $1, $&
        punctuation = []
        trailing_gt = ""

        if auto_linked?($`, $') || blockdown?($`,href)
          # do not change string; URL is already linked
          href
        else
          # don't include trailing punctuation character as part of the URL
          while href.sub!(/[^#{WORD_PATTERN}\/\-=;]$/, '')
            punctuation.push $&
            if opening = BRACKETS[punctuation.last] and href.scan(opening).size > href.scan(punctuation.last).size
              href << punctuation.pop
              break
            end
          end

          # don't include trailing &gt; entities as part of the URL
          trailing_gt = $& if href.sub!(/&gt;$/, '')

          link_text = block_given? ? yield(href) : href.sub(/^#{scheme}\/\//,'')
          href = 'http://' + href unless scheme

          unless options[:sanitize] == false
            link_text = sanitize(link_text)
            href      = sanitize(href)
          end
          content_tag(:a, link_text, link_attributes.merge('href' => href), !!options[:sanitize]) + punctuation.reverse.join('') + trailing_gt.html_safe
        end
      end
    end

    # Turns all email addresses into clickable links.
    def auto_link_email_addresses(text)
      text.gsub(AUTO_EMAIL_RE) do
        text = $&

        if auto_linked?($`, $')
          text
        else
          #display_text = (block_given?) ? yield(text) : text
          #display_text = text
          text.gsub!('@', '&#064').gsub!('.', '&#046;')
          %(<a href="mailto:#{text}">#{text}</a>)
        end
      end
    end

    # Detects already linked context or position in the middle of a tag
    def auto_linked?(left, right)
      (left =~ AUTO_LINK_CRE[0] and right =~ AUTO_LINK_CRE[1]) or
        (left.rindex(AUTO_LINK_CRE[2]) and $' !~ AUTO_LINK_CRE[3])
    end

    def blockdown?(before, href)
      (before.last == "[" && href =~ BLOCKDOWN_LINK_RE) ||
      (before.last == "<" && href.last == ">")
    end
  end
end