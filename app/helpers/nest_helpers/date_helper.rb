module NestHelpers
  module DateHelper
    #
    # display time ago in words, but with full timestamp in the hover
    #
    # include 'ago' if ago is true.
    #
    # TODO: how to localize? rails-i18n gem does not include an 'ago'
    #
    def time_ago(time, ago: false)
      #time_tag time, time_ago_in_words(time)
      suffix = ago ? " ago" : ""
      content_tag :time, class: 'hover', title: time.to_s, datetime: time.to_s do
        time_ago_in_words(time) + suffix
      end
    end

    def display_date(date)
      date = date.to_date unless date.is_a?(Date)
      content_tag :time do
        date.strftime("%Y-%m-%d")
      end
    end
  end
end