module NestHelpers
  module NavigationHelper
    #
    # returns the string 'active' if certain conditions are met.
    #
    # three forms:
    #
    # * array: active if the controller is any of the ones in the array
    # * hash: active if every condition in the hash is true
    # * array of hashes: each element is handled as a hash.
    #
    # examples:
    #
    #   active(controller: 'passwords')
    #   active(controller: 'passwords', action: 'show')
    #   active({controller: 'home'},{controller: 'root'})
    #
    # When using the hash form, :a is an alias for :action, and :c is an alias for :controller.
    #
    #    active(c: 'passwords')
    #
    def active(*args)
      if args.first.is_a?(Hash)
        args.each do |hsh|
          is_active = hsh.keys.inject(true) {|memo,i|
            param_key = i
            param_key = :controller if param_key == :c
            param_key = :action     if param_key == :a
            param_key = :path       if param_key == :p
            if param_key == :path
              match_against = request.path
            else
              match_against = params[param_key]
            end
            memo &&= begin
              if hsh[i].is_a? Regexp
                !!(hsh[i] =~ match_against)
              else
                [hsh[i]].flatten.include?(match_against)
              end
            end
          }
          if is_active
            return 'active'
          end
        end
        return nil
      else
        args.each do |name|
          if name.is_a? Regexp
            return 'active' if params[:controller] =~ name
          else
            name = name.to_s.sub(/^\//,'')
            return 'active' if params[:controller] == name
          end
        end
        return nil
      end
    end

    def nav_tab(label, path, klass, disabled: false)
      content_tag('li', class: 'nav-item') do
        link_to(label, path, class: ['nav-link', klass, disabled ? 'disabled' : nil].compact.join(' '))
      end
    end

    def nav_link(active:, path:, label:, icon:, disabled: false)
      if disabled
        content_tag(:span, class: "nav-link disabled #{active}") do
          icon(icon,label)
        end
      else
        link_to( icon(icon, label), path, class: "nav-link #{active}")
      end
    end

  end
end
