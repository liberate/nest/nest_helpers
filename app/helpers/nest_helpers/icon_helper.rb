#
# see https://icons.getbootstrap.com/
#
module NestHelpers
  module IconHelper

    # symbolic icon to literal icon map
    MAP = {
      "ok" => "check-circle",
      "cancel" => "x-circle",
      "add" => "plus-circle",
      "next" => "arrow-right-circle",
      "prev" => "arrow-left-circle"
      #"next" => "chevron-right",
      #"prev" => "chevron-left"
    }.freeze

    def icon(icon, label=nil, size: 16, color: nil, after: false)
      symbol = map(icon)
      if BootstrapIcons::BOOTSTRAP_ICONS_SYMBOLS[symbol]
        str = bootstrap_icon(symbol, width: size, height: size)
      else
        str = custom_icon(symbol, with: size, height: size)
      end
      if label
        if after
          str = h(label) + " " + str
        else
          str = str + " " + h(label)
        end
      end
      if color
        str = content_tag(:span, class: "text-#{color}") { str }
      end
      return str
    end

    def icon_after(icon, label, size: 16, color: nil)
      icon(icon, label, size: size, color: color, after: true)
    end

    def success_icon
      icon("check-circle", color: 'success')
    end

    def danger_icon
      icon("exclamation-circle", color: 'danger')
    end

    private

    def custom_icon(symbol, options = {})
      icon = NestHelpers::CustomIcon.new(symbol, options)
      content_tag(:svg, icon.path.html_safe, icon.options)
    end

    def map(icon)
      icon = icon.to_s
      MAP[icon] || icon
    end
  end
end